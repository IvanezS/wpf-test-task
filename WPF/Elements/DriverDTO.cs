﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WPF.Elements
{
    [Table("DRIVERS")]
    public class DriverDTO : BaseEntity
    {
        public string Metka { get; set; }
        public string? Person { get; set; }
    }
}
