﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WPF.Elements
{
    public class CarDriverINoty : BaseEntity, INotifyPropertyChanged
    {
        private string metka;
        private string? auto;
        private string? person;

        public string Metka
        {
            get { return metka; }
            set
            {
                metka = value;
                OnPropertyChanged("Metka");
            }
        }

        public string Auto
        {
            get { return auto; }
            set
            {
                auto = value;
                OnPropertyChanged("Auto");
            }
        }

        public string Person
        {
            get { return person; }
            set
            {
                person = value;
                OnPropertyChanged("Person");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public static implicit operator ObservableCollection<object>(CarDriverINoty v)
        {
            throw new NotImplementedException();
        }
    }
}
