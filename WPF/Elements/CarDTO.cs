﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WPF.Elements
{
    [Table("CARS")]
    public class CarDTO : BaseEntity
    {
        public string Metka { get; set; }
        public string? Auto { get; set; }

    }
}
