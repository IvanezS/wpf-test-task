﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using WPF.Elements;
using WPF.HostedServices;

namespace WPF
{
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        //private readonly AppHostedService _hs;
        private CarDriverINoty selectedRow;
        public ObservableCollection<CarDriverINoty> cds { get; set; }
        public CarDriverINoty SelectedRow { get { return selectedRow; } set { selectedRow = value; OnPropertyChanged("SelectedRow"); } }

        public ApplicationViewModel()
        {
            cds = new ObservableCollection<CarDriverINoty>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
