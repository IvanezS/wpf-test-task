﻿using System.Collections.Generic;

namespace WPF.Settings
{
    public class MainSettings
    {
            public List<string> AutosList { get; set; }
            public List<string> DriverList { get; set; }
            public long AutoIntervalMS { get; set; }
            public long DriversIntervalMS { get; set; }
            public int CreateNewLogFileIntervalMin { get; set; }
            public string LogFilesFolderPath { get; set; }
            public string DBFolderPath { get; set; }

    }
}
