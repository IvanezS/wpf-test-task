﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using WPF.Settings;

namespace WPF.Logging.FileLogger
{
    public class LogFileHostedService : IHostedService, IDisposable, ILogger
    {
        private Timer? _timer = null;
        private static string logFile { get; set; }
        static object _lock = new object();
        private Queue<string> LogDataQueue = new Queue<string>();
        private IOptions<MainSettings> _set { get; set; }

        public LogFileHostedService(IOptions<MainSettings> set) 
        {
            _set = set;
            Directory.CreateDirectory(_set.Value.LogFilesFolderPath);
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromMinutes(_set.Value.CreateNewLogFileIntervalMin));
            
            return Task.CompletedTask;
        }

        private void DoWork(object? state)
        {
            logFile = "Logs\\" + DateTime.Now.ToString("yyyy_MM_d_HH_mm_ss") + "_log.log";
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

        void ILogger.Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            lock (_lock)
            {
                LogDataQueue.Enqueue(formatter(state, exception) + Environment.NewLine);
                if (logFile != null) 
                {
                    string t = "";
                    foreach (string s in LogDataQueue) 
                    {
                        t += s;
                    }
                    
                    LogDataQueue.Clear();
                    File.AppendAllText(logFile, t);
                }
            }
        }

        bool ILogger.IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        IDisposable ILogger.BeginScope<TState>(TState state)
        {
            return this;
        }
    }


}

