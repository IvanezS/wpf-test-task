﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace WPF.Logging.FileLogger
{
    public class FileLoggerProvider : ILoggerProvider
    {
        private LogFileHostedService _logger;
        private bool disposedValue;
        private readonly IServiceProvider _sp;

        public FileLoggerProvider(IServiceProvider sp)
        {
            _sp = sp;
        }
        public ILogger CreateLogger(string categoryName)
        {
            _logger = _sp.GetRequiredService<LogFileHostedService>();

            return _logger;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~FileLoggerProvider()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
