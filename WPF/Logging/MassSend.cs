﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using WPF.Hubs;
using WPF.Logging.FileLogger;

namespace WPF.Logging
{
    public class MassSend : IDisposable
    {
        private readonly ILogger<MassSend> _logger;
        private bool disposedValue;

        public MassSend(ILogger<MassSend> logger, IHubContext<MessageHub> mesHub)
        {
            _logger = logger;
        }

        public void Send(string message)
        {
            var modifiedMessage = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff tt") + ": " + message;
            _logger.LogInformation(modifiedMessage);
        }

        public void SendWithDate(string Dt, string message)
        {
            if (Dt == null) Dt = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff tt");
            if (Dt != null)
            {
                var modifiedMessage = Dt + ": " + message;
                _logger.LogInformation(modifiedMessage);
            }
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~MassSend()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
