﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using System.Windows;
using WPF.Hubs;
using WPF.Logging.FileLogger;
using Microsoft.EntityFrameworkCore;
using WPF.Database;
using WPF.HostedServices;
using WPF.Logging;
using WPF.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;

namespace WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private IHost _host;

        protected override void OnStartup(StartupEventArgs e)
        {
            _host = Host.CreateDefaultBuilder(e.Args)
                .ConfigureWebHostDefaults(webHostBuilder => {
                    webHostBuilder.Configure(app =>
                    {
                        app.UseRouting();
                        app.UseEndpoints(endpoints =>
                        {
                            endpoints.MapHub<MessageHub>("/messageHub");
                        });
                    });
                })
                .ConfigureServices(services=>
                {
                    var config = services.BuildServiceProvider().GetRequiredService<IConfiguration>();
                    services.Configure<MainSettings>(config.GetSection("MainSettings"));
                    services.AddDbContext<AppDbContext>(options =>
                        options.UseSqlite(config.GetConnectionString("FactoryDbConnection"))
                            .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                    );

                    services.AddScoped<CarDriverRepository>();
                    services.AddScoped(typeof(IRepository<>), typeof(EFRepository<>));
                    services.AddScoped<IContextFactory, DbContextFactory>();
                    services.AddSingleton<LogFileHostedService>();
                    services.AddHostedService<LogFileHostedService>(provider => provider.GetService<LogFileHostedService>());
                    services.AddSingleton<DbWindowViewModel>();
                    services.AddSingleton<ApplicationViewModel>();
                    services.AddSingleton<AppHostedService>();
                    services.AddHostedService<AppHostedService>(provider => provider.GetService<AppHostedService>());

                    services.AddSignalR().AddJsonProtocol(options =>
                    {
                        options.PayloadSerializerOptions.PropertyNamingPolicy = null;
                    });
                    services.AddSingleton<MassSend>();
                    services.AddSingleton<MainWindow>();
                    services.AddSingleton<DbWindow>();
                })
                .ConfigureLogging(logging =>
                {
                    var sp = logging.Services.BuildServiceProvider();
                    logging.AddProvider(new FileLoggerProvider(sp));
                })
                .Build();
                
                _host.Start();

                _host.Services.GetRequiredService<MainWindow>().Show();
            
            base.OnStartup(e);
        }


        protected override void OnExit(ExitEventArgs e)
        {
            if (_host != null)
            {
                _host.StopAsync();
                _host.Dispose();
            }
            Current.Shutdown();
            base.OnExit(e);
        }
    }
}
