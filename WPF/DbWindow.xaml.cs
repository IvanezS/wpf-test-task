﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using WPF.Logging;

namespace WPF
{
    /// <summary>
    /// Interaction logic for _dbw.xaml
    /// </summary>
    public partial class DbWindow : Window
    {
        private readonly MassSend _ms;
        private readonly DbWindowViewModel _avm2;
        private static CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        public DbWindow(DbWindowViewModel avm2, MassSend ms)
        {
            _avm2 = avm2;
            _ms= ms;

            InitializeComponent();
            OtherInitialize();

            DataContext = _avm2;
            _ms.Send("Second Window Started");

            IsVisibleChanged += OnCV;

        }

        private void OnCV(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue) _ms.Send("Second Window is visible");
            if (!(bool)e.NewValue) _ms.Send("Second Window is hidden");
        }

        private void OtherInitialize()
        {
            this.Closing += new CancelEventHandler(this.Form1_Closing);
        }

        private void Form1_Closing(Object sender, CancelEventArgs e)
        {
          
            cancellationTokenSource.Cancel();
            this.Visibility = Visibility.Hidden;
            e.Cancel = true;
        }

        public void SecWindowFinish()
        {
            this.Closing -= new CancelEventHandler(this.Form1_Closing);
            this.Close();
        }

    }

    public class NullImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return DependencyProperty.UnsetValue;
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // According to https://msdn.microsoft.com/en-us/library/system.windows.data.ivalueconverter.convertback(v=vs.110).aspx#Anchor_1
            // (kudos Scott Chamberlain), if you do not support a conversion 
            // back you should return a Binding.DoNothing or a 
            // DependencyProperty.UnsetValue
            return Binding.DoNothing;
            // Original code:
            // throw new NotImplementedException();
        }
    }
}
