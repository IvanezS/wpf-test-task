﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using WPF.Elements;

namespace WPF
{
    public class DbWindowViewModel : INotifyPropertyChanged
    {
        private CarDriverINoty selectedRow;
        public ObservableCollection<CarDriverINoty> cds;
        public ObservableCollection<CarDriverINoty> CDS
        {
            get { return cds; }
            set
            {
                cds = value; OnPropertyChanged("CDS");
            }
        }
        public CarDriverINoty SelectedRow { get { return selectedRow; } set { selectedRow = value; OnPropertyChanged("SelectedRow"); } }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
