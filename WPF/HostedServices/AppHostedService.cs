﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using WPF.Database;
using WPF.Elements;
using WPF.Hubs;
using WPF.Settings;

namespace WPF.HostedServices
{
    public class AppHostedService : IHostedService, IDisposable
    {
        private Timer _timer;

        long autosIntervalMS { get; set; }
        long driversIntervalMS { get; set; }
        List<string> autos { get; set; }
        List<string> drivers { get; set; }
                
        static DateTime Dt { get; set; }

        long remainder;
        long MaxPhase;

        private Dispatcher d1 { get; set; }
                
        int AutoIndex { get; set; } = 0;
        int DriverIndex { get; set; } = 0;
        static bool AutosStarted = false;
        static bool DriversStarted = false;

        CancellationTokenSource ctAutos { get; set; }
        CancellationTokenSource ctDrivers { get; set; }
        static Action<List<string>>? a;
        static Action<List<string>>? b;
        private static Task? taskAutos;
        private static Task? taskDrivers;

        static Barrier barrier { get; set; } = new Barrier(1);

        private readonly ILogger<AppHostedService> _logger;
        private readonly IRepository<CarDTO> _contextCars;
        private readonly IRepository<DriverDTO> _contextDriver;
        private readonly CarDriverRepository _contextCarDriver;
        private readonly ApplicationViewModel _avm;
        private readonly DbWindowViewModel _avm2;
        private readonly IOptions<MainSettings> _set;
        private readonly IHubContext<MessageHub> _mesHub;

        public AppHostedService(ILogger<AppHostedService> logger,
                                IRepository<CarDTO> contextCars, 
                                IRepository<DriverDTO> contextDriver,
                                CarDriverRepository contextCarDriver,
                                ApplicationViewModel avm,
                                DbWindowViewModel avm2,
                                IOptions<MainSettings> set,
                                IHubContext<MessageHub> mesHub
        )
        {
            _logger = logger;
            _contextCars = contextCars;
            _contextDriver = contextDriver;
            _contextCarDriver= contextCarDriver;
            _avm = avm;
            _avm2 = avm2;
            _mesHub = mesHub;
            _set= set;

            _logger.LogInformation(DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff tt") + ": AppHostedService Constructor Started ");

            autosIntervalMS  = _set.Value.AutoIntervalMS;
            driversIntervalMS = _set.Value.DriversIntervalMS;
            autos = _set.Value.AutosList;
            drivers = _set.Value.DriverList;
            
            _ = Math.DivRem(Math.Max(autosIntervalMS, driversIntervalMS), Math.Min(autosIntervalMS, driversIntervalMS), out remainder);
            MaxPhase = (autosIntervalMS / remainder) * (driversIntervalMS / remainder);
            
            Dt = DateTime.Now;

            _avm.cds.Insert(0, new CarDriverINoty() { Metka = Dt.ToString("s"), Auto = autos[AutoIndex], Person = drivers[DriverIndex]});
            
            d1 = Dispatcher.CurrentDispatcher;
            try
            {
                _avm2.CDS = _contextCarDriver.GetAllAsync().Result;
            }
            catch
            {
                _logger.LogCritical("Can't read database");
            }
            
            a = (li) =>
            {
                AutosStarted = true;
                if (barrier != null) barrier.AddParticipant();
                while (!ctAutos.Token.IsCancellationRequested)
                {
                    try
                    {
                        for (int i2 = 0; i2 < autosIntervalMS / remainder; i2++)
                        {
                            sync();
                        }
                        AutoIndex = (AutoIndex >= li.Count - 1) ? 0 : AutoIndex + 1;
                        _contextCars.CreateAsync(new CarDTO() { Id = Dt.Ticks, Metka = Dt.ToString("s"), Auto = li[AutoIndex] }).ConfigureAwait(false);
                        SendMessage(1, $"Выбрано авто: {li[AutoIndex]}", Dt).ConfigureAwait(false);
                    }
                    catch (BarrierPostPhaseException bppe)
                    {
                        _logger.LogError("Caught BarrierPostPhaseException in Autos Task: {0}", bppe.Message);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Caught Exception in Autos Task: {0}", e.ToString());
                    }
                }
                
                if(barrier != null) barrier.RemoveParticipant();
                SendMessage(1, "Операция прервана", Dt).ConfigureAwait(false);
                AutosStarted = false;
            };

            b = (li) =>
            {
                DriversStarted = true;
                if (barrier != null) barrier.AddParticipant();

                while (!ctDrivers.Token.IsCancellationRequested)
                {
                    try
                    {
                        for (int i2 = 0; i2 < driversIntervalMS / remainder; i2++)
                        {
                            sync();
                        }

                        DriverIndex = (DriverIndex >= li.Count - 1) ? 0 : DriverIndex + 1;
                        _contextDriver.CreateAsync(new DriverDTO() { Id = Dt.Ticks, Metka = Dt.ToString("s"), Person = li[DriverIndex] }).ConfigureAwait(false);
                        SendMessage(2, $"Выбрано имя: {li[DriverIndex]}", Dt).ConfigureAwait(false); ;
                    }
                    catch (BarrierPostPhaseException bppe)
                    {
                        _logger.LogError("Caught BarrierPostPhaseException in Drivers Task: {0}", bppe.Message);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Caught Exception in Drivers Task: {0}", e.ToString());
                    }
                    
                }
                
                if (barrier != null) barrier.RemoveParticipant();
                SendMessage(2, $"Операция прервана", Dt).ConfigureAwait(false);
                DriversStarted = false;
            };

            StartAutos();
            StartDrivers();

        }

        void StartAutos()
        {
            if (taskAutos == null || !taskAutos.Status.Equals(TaskStatus.Running))
            {
                ctAutos = new CancellationTokenSource();
                taskAutos = Task.Factory.StartNew(() => { a(autos); }, ctAutos.Token);
            }
        }

        void StartDrivers()
        {
            if (taskDrivers == null || !taskDrivers.Status.Equals(TaskStatus.Running))
            {
                ctDrivers = new CancellationTokenSource();
                taskDrivers = Task.Factory.StartNew(() => { b(drivers); }, ctDrivers.Token);
            }
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Hosted Service running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromMilliseconds(remainder));
            
            return Task.CompletedTask;
        }


        private void DoWork(object state)
        {
            Dt = DateTime.Now;
            try
            {
                sync();
                if (barrier != null) SendMessage(3, "фаза: " + barrier.CurrentPhaseNumber + ", кол-во в sync: " + barrier.ParticipantsRemaining, Dt).ConfigureAwait(false);

                if (barrier != null)
                {
                    if (Math.Abs(barrier.CurrentPhaseNumber) % MaxPhase == 0 && barrier.ParticipantCount == 3) {
                        d1.InvokeAsync(new Action(() =>
                        {
                            _avm.cds.Insert(0, new CarDriverINoty() { Id = Dt.Ticks, Metka = Dt.ToString("s"), Auto = autos[AutoIndex], Person = drivers[DriverIndex] });
                        }));
                    }
                    if (barrier.CurrentPhaseNumber >= 6) RestartPhase();
                }
            }
            catch (BarrierPostPhaseException bppe)
            {
                _logger.LogError("Caught BarrierPostPhaseException in DoWork Task: {0}", bppe.Message);
            }
            catch (Exception e)
            {
                _logger.LogError("Caught Exception in DoWork Task: {0}", e.ToString());
            }
            try
            {
                _avm2.CDS = _contextCarDriver.GetAllAsync().Result;
            }
            catch (Exception ee)
            {
                _logger.LogCritical("Can't read database: " + ee.ToString());
            }
        }

        static Action sync = () =>
        {
            Task.Delay(10).Wait();
            if (barrier != null)
            {
                try
                {
                    barrier.SignalAndWait();
                }
                catch
                {
                    throw;
                }
            }
        };




        static void RestartPhase()
        {
            if (barrier != null) barrier.Dispose();
            barrier = new Barrier(1);
            if (AutosStarted) barrier.AddParticipant();
            if (DriversStarted) barrier.AddParticipant();
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Hosted Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);
            ctAutos.Cancel();
            ctDrivers.Cancel();
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }


        public void StartTask(int number)
        {
            if (number == 1)
            {
                while (true)
                {
                    if (barrier != null)
                    {
                        if (barrier.CurrentPhaseNumber % 2 == 0) break;
                    }
                }
                StartAutos();
            }
            if (number == 2)
            {
                while (true)
                {
                    if (barrier != null)
                    {
                        if (barrier.CurrentPhaseNumber % 3 == 0 ) break;
                    }
                }
                StartDrivers();
            }

        }

        public void StopTask(int number)
        {
            if (number == 1 && ctAutos != null && taskAutos != null)
            {
                ctAutos.Cancel();

            }
            if (number == 2 && ctDrivers != null && taskDrivers != null)
            {
                ctDrivers.Cancel();
            }
        }

        private async Task SendMessage(int LabelNumber, string mes, DateTime dt)
        {
            //Обновляем тексты в WPF
            MesEventArgs args = new MesEventArgs();
            args.Message = mes;
            args.LabelNumber = LabelNumber;
            OnMes(args);

            //Передаём SignalR клиентам
            if (LabelNumber != 3) await _mesHub.Clients.All.SendAsync("ReceiveMessage", dt.ToString("yyyy/MM/dd hh:mm:ss.fff tt") + ": " + mes);

            //Запишем и в логгер
            _logger.LogInformation(dt.ToString("yyyy/MM/dd hh:mm:ss.fff tt") + ": " + mes);
        }
        
        protected virtual void OnMes(MesEventArgs e)
        {
            EventHandler<MesEventArgs> handler = SendMes;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<MesEventArgs> SendMes;

    }

    public class MesEventArgs : EventArgs
    {
        public string? Message { get; set; }
        public int LabelNumber { get; set; }

    }

}

