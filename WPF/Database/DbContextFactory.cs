﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.IO;
using WPF.Settings;

namespace WPF.Database
{
    public class DbContextFactory : IContextFactory
    {
        private IConfiguration Configuration { get; set; }
        private IOptions<MainSettings> _set { get; set; }
        public DbContextFactory(IConfiguration configuration, IOptions<MainSettings> set)
        {
            Configuration = configuration;
            _set = set;
        }

        public AppDbContext Create()
        {
            Directory.CreateDirectory(_set.Value.DBFolderPath);

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlite(Configuration.GetConnectionString("FactoryDbConnection"))
                .Options;

            return new AppDbContext(options);
        }
    }
}
