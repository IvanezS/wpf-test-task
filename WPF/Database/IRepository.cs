﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WPF.Elements;

namespace WPF.Database
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task CreateAsync(T item);

        Task<IEnumerable<T>> GetAllAsync();
    }
}