﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using WPF.Elements;

namespace WPF.Database
{
    public interface IRepositoryCarDriver<T> where T : BaseEntity
    {
        Task<ObservableCollection<T>> GetAllAsync();
    }
}