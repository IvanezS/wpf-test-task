﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using WPF.Elements;

namespace WPF.Database
{
    public class CarDriverRepository : IRepositoryCarDriver<CarDriverINoty>
    {
        private readonly IContextFactory _dbContextFactory;
        public CarDriverRepository(IContextFactory dbContextFactory) 
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<ObservableCollection<CarDriverINoty>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                var cars = _db.Set<CarDTO>().ToList().TakeLast(1000);
                var drivers = _db.Set<DriverDTO>().ToList().TakeLast(1000);
                
                List<CarDriverINoty> list = new List<CarDriverINoty>();
                foreach (var car in cars)
                {
                    var cd = new CarDriverINoty() {Id = car.Id, Metka = car.Metka, Auto = car.Auto };
                    var rl = drivers.Where(m=>m.Metka == car.Metka).FirstOrDefault();
                    if (rl != null) cd.Person = rl.Person;
                    list.Add(cd);

                }

                foreach (var driver in drivers)
                {
                    var fm = list.Where(x => x.Metka == driver.Metka).FirstOrDefault();
                    if (fm == null)
                    {
                        list.Add(fm = new CarDriverINoty() {Id = driver.Id, Metka = driver.Metka, Person = driver.Person });
                    }
                }
                var d = list.OrderByDescending(x => x.Id).Take(1000);
                ObservableCollection<CarDriverINoty> olist = new ObservableCollection<CarDriverINoty>();
                foreach (var cd in d) olist.Add(new CarDriverINoty() { Id = cd.Id, Auto = cd.Auto, Metka = cd.Metka, Person = cd.Person });


                return olist;
            }
        }



    }
}
