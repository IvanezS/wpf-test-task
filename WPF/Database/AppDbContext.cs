﻿using Microsoft.EntityFrameworkCore;
using WPF.Elements;

namespace WPF.Database
{
    public class AppDbContext : DbContext
    {

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<CarDTO> Cars { get; set; }
        public DbSet<DriverDTO> Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CarDTO>().ToTable("CARS").HasComment("Generated Cars table").HasKey(x => x.Id);
            modelBuilder.Entity<CarDTO>().Property(x => x.Metka).IsRequired(true).HasComment("Record index time");
            modelBuilder.Entity<CarDTO>().Property(x => x.Auto).IsRequired(false).HasComment("Model of car");


            modelBuilder.Entity<DriverDTO>().ToTable("DRIVERS").HasComment("Generated Drivers table").HasKey(x => x.Id);
            modelBuilder.Entity<DriverDTO>().Property(x => x.Metka).IsRequired(true).HasComment("Record index time");
            modelBuilder.Entity<DriverDTO>().Property(x => x.Person).IsRequired(false).HasComment("Model of car");

            base.OnModelCreating(modelBuilder);
        }



    }
}
