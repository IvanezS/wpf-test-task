﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using WPF.Elements;

namespace WPF.Database
{
    public class EFRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IContextFactory _dbContextFactory;
        private readonly object Lock = new object();
        public EFRepository(IContextFactory dbContextFactory) 
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task CreateAsync(T item)
        {
            lock (Lock)
            {
                using (var _db = _dbContextFactory.Create())
                {
                    _db.Set<T>().Add(item);
                    try
                    {
                        _db.SaveChangesAsync();
                    }
                    catch {
                        throw;
                    }
                }
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            using (var _db = _dbContextFactory.Create())
            {
                return await _db.Set<T>().ToListAsync();
            }
        }
    }
}
