﻿namespace WPF.Database
{
    public interface IContextFactory
    {
        AppDbContext Create();
    }
}