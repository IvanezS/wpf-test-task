﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using WPF.Logging;
using WPF.HostedServices;

namespace WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static CancellationTokenSource ct { get; set; } = new CancellationTokenSource();
        private DbWindow _dbw;
        private readonly AppHostedService _hs;
        private readonly ApplicationViewModel _avm;
        private readonly MassSend _ms;

        public MainWindow(  MassSend ms,
                            AppHostedService hs,
                            DbWindow dbw,
                            ApplicationViewModel avm)
        {
            _hs = hs;
            _dbw= dbw;
            _avm = avm;
            _ms = ms;

            Closed += OnWindowClosing;
            _hs.SendMes += ParseMessage;

            InitializeComponent();
            
            DataContext = _avm;

            _ms.Send("Main Window Started");
        }

        void OnWindowClosing(object sender, EventArgs e)
        {
            Close();
            _ms.Send("Main Window Closed");
            _dbw.SecWindowFinish();
            _ms.Send("Second Window Closed");
            _hs.StopAsync(ct.Token);
        }

        private void OnSecWindowClosing(object sender, CancelEventArgs e)
        {
            ct.Cancel();
        }

        void ParseMessage(object sender, MesEventArgs e)
        {
            if (e.LabelNumber == 1) Task.Run(async () => await UdpateLabel(t1Label, e.Message));
            if (e.LabelNumber == 2) Task.Run(async () => await UdpateLabel(t2Label, e.Message));
            if (e.LabelNumber == 3) Task.Run(async () => await UdpateLabel(lblTime, e.Message));  

        }

        async Task UdpateLabel(System.Windows.Controls.Label l, string v)
        {
            if(l!=null)

                _ = Dispatcher.InvokeAsync(new Action(() =>
                {
                    l.Content = v;
                }));
        }

        private async void StartTread1Button_Click(object sender, RoutedEventArgs e)
        {
            _ms.Send("User pressed /START THREAD 1 (Cars)/ button");
            _hs.StartTask(1);
        }

        private void StopTread1Button_Click(object sender, RoutedEventArgs e)
        {
            _ms.Send("User pressed /STOP THREAD 1 (Cars)/ button");
            _hs.StopTask(1);
        }

        private void StartTread2Button_Click(object sender, RoutedEventArgs e)
        {
            _ms.Send("User pressed /START THREAD 2 (Names)/ button");
            _hs.StartTask(2);
        }

        private void StopTread2Button_Click(object sender, RoutedEventArgs e)
        {
            _ms.Send("User pressed /STOP THREAD 2 (Names)/ button");
            _hs.StopTask(2);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _ms.Send("User pressed /OPEN SECOND Window/ button");
            _dbw.Visibility = Visibility.Visible;
        }

    }



}
