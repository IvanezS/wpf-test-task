using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Linq;
using WPF.Database;
using WPF.Elements;
using WPF.IntagrationTests.Database;
using Xunit;

namespace WPF.IntagrationTests
{
    public class CarDriverRepositoryTests : IClassFixture<TestDatabaseFixture>
    {
        private TestDatabaseFixture _fixture;
        private CarDriverRepository repo;
        private ObservableCollection<CarDriverINoty> list;

        public CarDriverRepositoryTests(TestDatabaseFixture fixture)
        {
            _fixture = fixture;
            repo = new CarDriverRepository(_fixture.dbFactory);

            //Act
            ObservableCollection<CarDriverINoty> cdlist = repo.GetAllAsync().Result;
            list = new ObservableCollection<CarDriverINoty>();
            foreach (var cd in cdlist) list.Add(cd);
        }

        [Fact]
        public void GetAllAsync_Return_NotNull_CarDriver_List()
        {
            //Assert
            Assert.NotNull(list);
        }

        [Fact]
        public void GetAllAsync_Return_5rows_in_CarDriver_List()
        {
            //Assert
            Assert.Equal(5, list.Count);
        }

        [Fact]
        public void GetAllAsync_Return_Expected_CarDriver_List()
        {
            ObservableCollection<CarDriverINoty> FakeList = new ObservableCollection<CarDriverINoty>
            {
                new CarDriverINoty() { Auto = "���", Id = 10000, Metka = "0", Person = "������" },
                new CarDriverINoty() { Auto = "����", Id = 12000, Metka = "2", Person = null },
                new CarDriverINoty() { Auto = null, Id = 13000, Metka = "3", Person = "��������" },
                new CarDriverINoty() { Auto = "���", Id = 14000, Metka = "4", Person = null },
                new CarDriverINoty() { Auto = "����", Id = 16000, Metka = "6", Person = "�������" },
            };

            var obj1Str = JsonConvert.SerializeObject(FakeList.OrderByDescending(x=>x.Id));
            var obj2Str = JsonConvert.SerializeObject(list.OrderByDescending(x => x.Id));

            //Assert
            Assert.Equal(obj1Str, obj2Str);


        }



    }
}
