﻿using System;
using WPF.Database;
using WPF.Elements;

namespace WPF.IntagrationTests.Database
{
    public class TestDatabaseFixture : IDisposable
    {
        public IContextFactory dbFactory { get; private set; } = new DbContextFactoryTest();
        public AppDbContext dbContext { get; private set; } 

        public TestDatabaseFixture()
        {
            dbContext = (AppDbContext)dbFactory.Create();
            InitializeDb();
            Dispose();
        }
        public void InitializeDb()
        {
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();

            dbContext.Cars.Add(new CarDTO() { Metka = "0", Auto = "Уаз", Id = 10000 });
            dbContext.Cars.Add(new CarDTO() { Metka = "2", Auto = "Жига", Id = 12000 });
            dbContext.Cars.Add(new CarDTO() { Metka = "4", Auto = "ЗиЛ", Id = 14000 });
            dbContext.Cars.Add(new CarDTO() { Metka = "6", Auto = "Урал", Id = 16000 });
            dbContext.SaveChanges();

            dbContext.Persons.Add(new DriverDTO() { Metka = "0", Person = "Марина", Id = 10000 });
            dbContext.Persons.Add(new DriverDTO() { Metka = "3", Person = "Феодосий", Id = 13000 });
            dbContext.Persons.Add(new DriverDTO() { Metka = "6", Person = "Николай", Id = 16000 });

            dbContext.SaveChanges();

        }
        public void CleanDb()
        {
            dbContext.Database.EnsureDeleted();
        }
        public void Dispose()
        {
            dbContext.Dispose();
        }
    }
}
