﻿using Microsoft.EntityFrameworkCore;
using WPF.Database;

namespace WPF.IntagrationTests.Database
{
    public class DbContextFactoryTest : IContextFactory
    {
        public AppDbContext Create()
        {

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDB")
                .Options;

            return new AppDbContext(options);
        }
    }
}
